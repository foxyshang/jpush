package foxy;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.Timer;
import java.util.TimerTask;

import foxy.jpush.Constant;
import foxy.jpush.JsonStruct;
import foxy.jpush.Utils;
import foxy.niosocket.NioSocketConnection;
import foxy.niosocket.NioSocketConnectionHandler;

public class Test {
	private static Timer timer=new Timer();
	private static boolean isConnect=false;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		final NioSocketConnection connection=new NioSocketConnection();
		  Timer timer=new Timer();
		  connect(connection);
		  timer.schedule(new TimerTask() {
				@Override
				public void run() {
					if(connection.isConnected()){
						if(isConnect){
						 connection.sendPingFrame();
						 System.out.println("ping"  
			                     ); 
						}else{
							 JsonStruct jsonStruct=new JsonStruct("48947381","1","samsung","6a813f733b974657bd99c02bae32c782","83EB8022CEF82135EA5AE3D627D18026","10225");
							 byte[] data=Utils.getDataFromJson(jsonStruct.toJsonString());
							 byte[] result=Utils.getSendPagectFromInt(data, Constant.DataType.LPUSH_HEADER_TYPE_HANDSHAKE);
							 connection.sendBinaryMessage(result);
							 Utils.printHexString(result);
						}
						
					}
					else{
						 connect(connection);
					}
				}
			}, 10000,10000);

	}
	
	private static void connect(final NioSocketConnection connection){
		connection.connect("172.16.104.21:9732", new NioSocketConnectionHandler() {
			@Override
			public void onTextMessage(String payload) {
				
			}
			
			@Override
			public void onRawTextMessage(byte[] payload) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onOpen() {
				 System.out.println("open");
				 
				 
				// connection.sendTextMessage("发送数据qareghwt4hewtyjhetyjhe5yj");
				 JsonStruct jsonStruct=new JsonStruct("48947381","1","samsung","6a813f733b974657bd99c02bae32c782","83EB8022CEF82135EA5AE3D627D18026","10225");
				 byte[] data=Utils.getDataFromJson(jsonStruct.toJsonString());
				 byte[] result=Utils.getSendPagectFromInt(data, Constant.DataType.LPUSH_HEADER_TYPE_HANDSHAKE);
				 connection.sendBinaryMessage(result);
				 Utils.printHexString(result);
			}
			
			@Override
			public void onClose(int code, String reason) {
				 System.out.println("onClose\n"+"code:"+code+"\n reason:"+reason);  
				 isConnect=false;
				
			}
			
			@Override
			public void onBinaryMessage(byte[] payload) {
				 System.out.println("接收到来自服务器 的信息:"  
	                     + Arrays.toString(payload));  
				 if(payload.length>14){
					 byte[] header=new byte[5];
					 System.arraycopy(payload, 0, header,0, 5);	
					 if(Arrays.equals(header, Constant.FLAG) ){
						if(payload[9]==0x01){
							byte[] result=null;
							try {
								result = Utils.getSendPagectFromInt(Utils.stringToByte("createConnection"), Constant.DataType.LPUSH_HEADER_TYPE_CREATE_CONNECTION);
							} catch (UnsupportedEncodingException e) {
								e.printStackTrace();
							}
							 connection.sendBinaryMessage(result);
						}else if(payload[9]==0x04){
							isConnect=true;
							connection.sendPingFrame();
						}else if(payload[9]==0x06){
							timer.cancel();
							timer.purge();
							timer=null;
							System.out.println("定时器取消了");  
						}
					 }
					 
				 }
				
			}
		});
	}

}

package foxy.jpush;

public class Constant {
	public static final byte[] FLAG={'L','P','U','S','H'};
	
	/*#define LPUSH_HEADER_TYPE_TEST[A4]             0x02
	#define LPUSH_HEADER_TYPE_REQUEST_SOURCE[A5]       0x03
	#define LPUSH_HEADER_TYPE_CREATE_CONNECTION[A6]       0x04
	#define LPUSH_HEADER_TYPE_HREATBEAT[A7]            0x05
	#define LPUSH_HEADER_TYPE_CLOSE[A8]            0x06
	#define LPUSH_HEADER_TYPE_TEST_PUSH[A9]            0x07
	#define LPUSH_HEADER_TYPE_PUSH  [A10]           0x08*/
	
	
	public static class  DataType{
		public static final byte LPUSH_HEADER_TYPE_HANDSHAKE=0x01;
		public static final byte LPUSH_HEADER_TYPE_TEST=0x02;
		public static final byte LPUSH_HEADER_TYPE_REQUEST_SOURCE=0x03;
		public static final byte LPUSH_HEADER_TYPE_CREATE_CONNECTION=0x04;
		public static final byte LPUSH_HEADER_TYPE_HREATBEAT=0x05;
		public static final byte LPUSH_HEADER_TYPE_CLOSE=0x06;
		public static final byte LPUSH_HEADER_TYPE_TEST_PUSH=0x07;
		public static final byte LPUSH_HEADER_TYPE_PUSH=0x08;
	}
	
	/*LPUSH_FMT_STRING     [A14]     0x01    
	 LPUSH_FMT_BOOL[A15]        0x02         datalen 1byte
	 LPUSH_FMT_INT[A16]                  0x03     datalen 4byte
	 LPUSH_FMT_LONG[A17]        0x04         datalen  8byte
	 LPUSH_FMT_FLOAT[A18]            0x05         datalen  8byte
	 LPUSH_FMT_JSON[A19]         0x06*/
	public static class MessageType{
		public static final byte LPUSH_FMT_STRING =0x01;
		public static final byte LPUSH_FMT_BOOL=0x02;
		public static final byte LPUSH_FMT_INT =0x03;
		public static final byte LPUSH_FMT_LONG=0x04;
		public static final byte LPUSH_FMT_FLOAT=0x05;
		public static final byte  LPUSH_FMT_JSON=0x06;
	
	}

}

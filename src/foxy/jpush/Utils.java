package foxy.jpush;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;

public class Utils {
	public static byte[] getTimeStamp(){
		Date date=new Date();
		int time=(int) (date.getTime()/1000);
		return intToBytes(time);
	}
	
	//long类型转成byte数组  
	public static byte[] longToBytes(long number) {  
	        long temp = number;  
	        byte[] b = new byte[8];  
	        for (int i = 0; i < b.length; i++) {  
	            b[i] = new Long(temp & 0xff).byteValue();// 将最低位保存在最低位  
	        temp = temp >> 8; // 向右移8位  
	    }  
	    return b;  
	}  
	  
	//byte数组转成long  
	public static long bytesToLong(byte[] b) {  
	  
	    long s = 0;  
	    long s0 = b[0] & 0xff;// 最低位  
	    long s1 = b[1] & 0xff;  
	    long s2 = b[2] & 0xff;  
	    long s3 = b[3] & 0xff;  
	    long s4 = b[4] & 0xff;// 最低位  
	    long s5 = b[5] & 0xff;  
	    long s6 = b[6] & 0xff;  
	    long s7 = b[7] & 0xff;  
	  
	    // s0不变  
	    s1 <<= 8;  
	    s2 <<= 16;  
	    s3 <<= 24;  
	    s4 <<= 8 * 4;  
	    s5 <<= 8 * 5;  
	    s6 <<= 8 * 6;  
	    s7 <<= 8 * 7;  
	    s = s0 | s1 | s2 | s3 | s4 | s5 | s6 | s7;  
	    return s;  
	}  
	  
	public static byte[] intToBytes(int value) {  
	      
		   
		    byte[] src = new byte[4];    
		    src[0] = (byte) ((value>>24) & 0xFF);    
		    src[1] = (byte) ((value>>16)& 0xFF);    
		    src[2] = (byte) ((value>>8)&0xFF);      
		    src[3] = (byte) (value & 0xFF);         
		    return src;    
		

	}  
	  
	public static int bytesToInt(byte[] b) {  
	  
	    int s = 0;  
	    int s0 = b[0] & 0xff;// 最低位  
	    int s1 = b[1] & 0xff;  
	    int s2 = b[2] & 0xff;  
	    int s3 = b[3] & 0xff;  
	    s3 <<= 24;  
	    s2 <<= 16;  
	    s1 <<= 8;  
	    s = s0 | s1 | s2 | s3;  
	    return s;  
	}  
	  
	//浮点到字节转换  
	public static byte[] doubleToBytes(double d)  
	{  
	    byte writeBuffer[]= new byte[8];  
	     long v = Double.doubleToLongBits(d);  
	        writeBuffer[0] = (byte)(v >>> 56);  
	        writeBuffer[1] = (byte)(v >>> 48);  
	        writeBuffer[2] = (byte)(v >>> 40);  
	        writeBuffer[3] = (byte)(v >>> 32);  
	        writeBuffer[4] = (byte)(v >>> 24);  
	        writeBuffer[5] = (byte)(v >>> 16);  
	        writeBuffer[6] = (byte)(v >>>  8);  
	        writeBuffer[7] = (byte)(v >>>  0);  
	        return writeBuffer;  
	  
	}  
	  
	//字节到浮点转换  
	public static double bytesToDouble(byte[] readBuffer)  
	{  
	     return Double.longBitsToDouble((((long)readBuffer[0] << 56) +  
	                ((long)(readBuffer[1] & 255) << 48) +  
	                ((long)(readBuffer[2] & 255) << 40) +  
	                ((long)(readBuffer[3] & 255) << 32) +  
	                ((long)(readBuffer[4] & 255) << 24) +  
	                ((readBuffer[5] & 255) << 16) +  
	                ((readBuffer[6] & 255) <<  8) +  
	                ((readBuffer[7] & 255) <<  0))  
	          );  
	}  
	
	public static byte[] stringToByte(String json) throws UnsupportedEncodingException{
		return json.getBytes("UTF8");
	}
	
	public static byte[] arrayIntToByte(int[] numbers) throws UnsupportedEncodingException{
		byte[] bytes=new byte[numbers.length*4];
		for(int i=0;i<numbers.length;i++){
			System.arraycopy(intToBytes(numbers[i]),0, bytes,4*i , 4);
		}
		return bytes;
	}
	
	
	
	
	
	public static byte[] getDataFromJson(String json){
		byte[] result=null;
		try {
			byte[] jsonBytes=stringToByte(json);
			result=new byte[jsonBytes.length+5];
			result[0]=Constant.MessageType.LPUSH_FMT_JSON;
			int length=jsonBytes.length;
			System.arraycopy(intToBytes(length), 0, result, 1, 4);
			System.arraycopy(jsonBytes, 0, result, 5, length);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return result;
		
	}
	
	
	public static byte[] getDataFromInt(int[] data){
		byte[] result=new byte[4*data.length+5];
		result[0]=Constant.MessageType.LPUSH_FMT_INT;
		int length=4*data.length;
		System.arraycopy(intToBytes(length), 0, result, 1, 4);
		try {
			System.arraycopy(arrayIntToByte(data), 0, result, 5, length);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} 
		
		return result;
	}
	
	public static byte[] getSendPagectFromInt(byte[] data,byte type){
		byte[] result=new byte[data.length+14];
		System.arraycopy(Constant.FLAG, 0, result, 0, 5);		//falg
		System.arraycopy(getTimeStamp(), 0, result, 5, 4);		//timestamp
		//System.arraycopy(type, 0, result, 9, 1);		//type
		result[9]=type;
		System.arraycopy(intToBytes(data.length), 0, result,10, 4);		//datalength
		System.arraycopy(data, 0, result,14, data.length);	
		return result;
	}
	 //静态方法，便于作为工具类  
	    public static String getMd5(String plainText) {  
	          try {  
	              MessageDigest md = MessageDigest.getInstance("MD5");  
	             md.update(plainText.getBytes());  
            byte b[] = md.digest();  
	   
	             int i;  
	   
	             StringBuffer buf = new StringBuffer("");  
	             for (int offset = 0; offset < b.length; offset++) {  
	                 i = b[offset];  
	                 if (i < 0)  
	                     i += 256;  
	               if (i < 16)  
	                     buf.append("0");  
	                 buf.append(Integer.toHexString(i));  
	             }  
	             //32位加密  
	             return buf.toString();  
	             // 16位的加密  
            // return buf.toString().substring(8, 24);  
	         } catch (NoSuchAlgorithmException e) {  
	             e.printStackTrace();  
	             return null;  
	         }  
	   
	      }  
	    
	    public static void printHexString(byte[] b)
	    {
	       
	        for (int i = 0; i < b.length; i++)
	        {
	            String hex = Integer.toHexString(b[i] & 0xFF);
	            if (hex.length() == 1)
	            {
	                hex = '0' + hex;
	            }
	            System.out.print(hex.toUpperCase() + " ");
	        }
	        System.out.println("");
	    } 

	

}

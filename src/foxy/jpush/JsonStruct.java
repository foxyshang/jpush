package foxy.jpush;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

public class JsonStruct {
	private String appId="";
	private String clientFlag="";
	private String devices="";
	private String identity="";
	private String md5Data="";
	private String screteKey="";
	private String userId="";
	public String getAppId() {
		return appId;
	}
	public void setAppId(String appId) {
		this.appId = appId;
	}
	public String getClientFlag() {
		return clientFlag;
	}
	public void setClientFlag(String clientFlag) {
		this.clientFlag = clientFlag;
	}
	public String getDevices() {
		return devices;
	}
	public void setDevices(String devices) {
		this.devices = devices;
	}
	public String getIdentity() {
		return identity;
	}
	public void setIdentity(String identity) {
		this.identity = identity;
	}
	public String getMd5Data() {
		return md5Data;
	}
	public void setMd5Data(String md5Data) {
		this.md5Data = md5Data;
	}
	public String getScreteKey() {
		return screteKey;
	}
	public void setScreteKey(String screteKey) {
		this.screteKey = screteKey;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	public String  toJsonString(){
		JSONObject jsonObject=new JSONObject();
		jsonObject.put("appId", appId);
		jsonObject.put("clientFlag", clientFlag);
		jsonObject.put("devices", devices);
		jsonObject.put("identity", identity);
		jsonObject.put("md5Data", md5Data);
		jsonObject.put("screteKey", screteKey);
		jsonObject.put("userId", userId);
		return jsonObject.toString();
		
	}
	public JsonStruct(String appId, String clientFlag, String devices,
			String identity,  String screteKey, String userId) {
		super();
		this.appId = appId;
		this.clientFlag = clientFlag;
		this.devices = devices;
		this.identity = identity;
		this.screteKey = screteKey;
		this.userId = userId;
		this.md5Data=Utils.getMd5(this.userId+this.appId+this.screteKey);
		
	}
	
	
	
	

}

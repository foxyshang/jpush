package foxy.niosocket;

import foxy.niosocket.NioSocket.ConnectionHandler;

public class NioSocketConnectionHandler implements ConnectionHandler{

	@Override
	public void onOpen() {
		
	}

	@Override
	public void onClose(int code, String reason) {
		
	}

	@Override
	public void onTextMessage(String payload) {
		
	}

	@Override
	public void onRawTextMessage(byte[] payload) {
		
	}

	@Override
	public void onBinaryMessage(byte[] payload) {
		
	}


}

package foxy.niosocket;

public interface NioSocket {
   
	/**
    * Session handler for Socket sessions.
    */
   public interface ConnectionHandler {

	   /**
	    * Connection was closed normally.
	    * 正常断开连接
	    */
	   public static final int CLOSE_NORMAL = 1;

	   /**
	    * Connection could not be established in the first place.
	    * 无法建立连接
	    */
	   public static final int CLOSE_CANNOT_CONNECT = 2;

	   /**
	    * A previously established connection was lost unexpected.
	    * 原来的北断开
	    */
	   public static final int CLOSE_CONNECTION_LOST = 3;

	   /**
	    * The connection was closed because a protocol violation
	    * occurred.
	    * 违反协议被断开
	    */
	   public static final int CLOSE_PROTOCOL_ERROR = 4;

	   /**
	    * Internal error.
	    * 内部错误
	    */
	   public static final int CLOSE_INTERNAL_ERROR = 5;
	   
	   /**
	    * Server returned error while connecting
	    * 连接时服务器返回错误而关闭
	    */
	   public static final int CLOSE_SERVER_ERROR = 6;
	   
	   /**
	    * Server connection lost, scheduled reconnect
	    * 服务器连接丢失重新连接
	    */
	   public static final int CLOSE_RECONNECT = 7;

	   /**
	    * Fired when the WebSockets connection has been established.
	    * After this happened, messages may be sent.
	    * 服务器已经确定来连接成功，可以发送信息
	    */
	   public void onOpen();

	   /**
	    * 连接已经被断开
	    * Fired when the WebSockets connection has deceased (or could
	    * not established in the first place).
	    *
	    * @param code       Close code.
	    * @param reason     Close reason (human-readable).
	    */
	   public void onClose(int code, String reason);

	   /**
	    * Fired when a text message has been received (and text
	    * messages are not set to be received raw).
	    * 接收到消息时不是原生的数据
	    *
	    * @param payload    Text message payload or null (empty payload).
	    */
	   public void onTextMessage(String payload);

	   /**
	    * Fired when a text message has been received (and text
	    * messages are set to be received raw).
	    * 可以被utf-8解析的byte数据
	    * @param payload    Text message payload as raw UTF-8 or null (empty payload).
	    */
	   public void onRawTextMessage(byte[] payload);

	   /**
	    * Fired when a binary message has been received.
	    * 所有二进制数据
	    * @param payload    Binar message payload or null (empty payload).
	    */
	   public void onBinaryMessage(byte[] payload);
	   
   }

   public void connect(String wsUri, ConnectionHandler wsHandler) ;
   public void disConnect();
   public boolean isConnected();
   public void sendBinaryMessage(byte[] payload);
   public void sendRawTextMessage(byte[] payload);
   public void sendTextMessage(String payload);
	public void sendPingFrame();
}
